FROM golang:alpine
MAINTAINER Tobias Kilb tobias@kilb.org

WORKDIR /build
COPY . .

RUN go build -o tkwiki .

WORKDIR /dist
COPY static ./static/
COPY wtmpl ./wtmpl/
RUN cp /build/tkwiki .

EXPOSE 8080
CMD ["/dist/tkwiki", "-datadir", "/tkdata/", "-listen", "0.0.0.0:8080"] 
