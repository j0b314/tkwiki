/*
 * The tkwiki main module
 *
 * author: j0b314 (tobias@kilb.org)
 *
 */

package main

import (
	"flag"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"regexp"
	"strings"
	"time"
)

// Default config
var pagedir = "data/"
var tmpldir = "wtmpl/"
var staticdir = "static/"
var defaultlistenaddr = "127.0.0.1:8080"

// Template function map
var funcMap = template.FuncMap{
	"genLinks": TransformLink,
}

// Main template
var templates = template.New("main")

// Regexp for prevent accessing files outside pagedir
var validPath = regexp.MustCompile("^/(edit|save|view|delete|info)/([a-zA-Z0-9]+)$")

// Regexp for searching links inside body text like
// "This is a [link] to another page"
var validInterLink = regexp.MustCompile("\\[[a-zA-Z0-9]+\\]")

// The layout of a wiki page
type Page struct {
	Title        string
	Body         []byte
	LastModified time.Time
}

// Handler for saving pages persistent
func (p *Page) save() error {
	filename := pagedir + p.Title + ".txt"
	return ioutil.WriteFile(filename, p.Body, 0644)
}

// This is called by the regex ReplaceAllFunc which replaces
// a wiki syntax link "[[ALink]]" with a
// html link "<a href..."
func replaceLink(data []byte) []byte {
	indata := string(data)
	indata = strings.TrimPrefix(indata, "[")
	indata = strings.TrimSuffix(indata, "]")

	link := "<a href=\"/view/" + indata + "\">" + indata + "</a>"
	return []byte(link)
}

// Find all wiki links in document via regex
func findLink(in string) string {
	return string(validInterLink.ReplaceAllFunc([]byte(in), replaceLink))
}

// This is a wrapper function for escape all html/script/css
// in document and then convert wiki links to html links
func TransformLink(in []byte) template.HTML {
	work := string(in)
	work = template.HTMLEscaper(work)
	work = findLink(work)
	return template.HTML(work)
}

// Handler for loading pages from persistent memory
func loadPage(title string) (*Page, error) {
	filename := pagedir + title + ".txt"
	body, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	meta, err := os.Lstat(filename)
	if err != nil {
		fmt.Println("Err: ", err)
		return nil, err
	}
	return &Page{Title: title, Body: body, LastModified: meta.ModTime()}, err
}

// Wrapping the handler calls into a function understood by
// the html package
func makeHandler(fn func(http.ResponseWriter, *http.Request, string)) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		m := validPath.FindStringSubmatch(r.URL.Path)
		if m == nil {
			http.NotFound(w, r)
			return
		}
		fn(w, r, m[2])
	}
}

// Render the given template
func renderTemplate(w http.ResponseWriter, tmpl string, p *Page) {
	err := templates.ExecuteTemplate(w, tmpl+".html", p)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

// Handler for viewing the wiki pages
func viewHandler(w http.ResponseWriter, r *http.Request, title string) {
	p, err := loadPage(title)
	if err != nil {
		http.Redirect(w, r, "/edit/"+title, http.StatusFound)
		return
	}
	renderTemplate(w, "view", p)
}

// Handler for creating or editing wiki pages
func editHandler(w http.ResponseWriter, r *http.Request, title string) {
	p, err := loadPage(title)
	if err != nil {
		p = &Page{Title: title}
	}
	renderTemplate(w, "edit", p)
}

// Handler for saving wiki pagedata
func saveHandler(w http.ResponseWriter, r *http.Request, title string) {
	body := r.FormValue("body")
	p := &Page{Title: title, Body: []byte(body)}
	err := p.save()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	http.Redirect(w, r, "/view/"+title, http.StatusFound)
}

// Handler for deleting pages from the wiki
func deleteHandler(w http.ResponseWriter, r *http.Request, title string) {
	deltimestamp := time.Now().UTC().String()
	oldfile := pagedir + title + ".txt"
	newfile := pagedir + "trash/" + title + "_" + deltimestamp + ".txt"

	// Read old file
	data, err := ioutil.ReadFile(oldfile)
	if err != nil {
		log.Println("Error reading file marked for deletion: ", oldfile)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Write file to trash
	err = ioutil.WriteFile(newfile, data, 0755)
	if err != nil {
		log.Println("Error writing file in trash folder: ", oldfile)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Remove file from wiki index
	err = os.Remove(oldfile)
	if err != nil {
		log.Println("Error deleting file: ", oldfile)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Forward user to FrontPage
	http.Redirect(w, r, "/view/FrontPage", http.StatusFound)

}

// Handler for getting the metadata of a page
func metadataHandler(w http.ResponseWriter, r *http.Request, title string) {
	p, err := loadPage(title)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	log.Println("metadata viewing not implemented [" + p.Title + "]")
}

// Root wiki page
func rootPage(w http.ResponseWriter, r *http.Request) {

	http.Redirect(w, r, "/view/FrontPage", http.StatusFound)
}

// Checks if the datadir needs to be created
func initDataDir() {
	err := os.Mkdir(pagedir, 0755)
	if err != nil {
		if os.IsExist(err) {
			log.Println("Wiki data folder: [" + pagedir + "]")
			return
		} else {
			log.Fatal("Error: ", err)
		}
		log.Println("Creating [" + pagedir + "] as wiki data dir")
	}
}

// Checks if the trash under the datadir needs to be created
func initTrash() {
	err := os.Mkdir(pagedir+"trash", 0755)
	if err != nil {
		if os.IsExist(err) {
			log.Println("Page trash folder: [" + pagedir + "trash]")
			return
		} else {
			log.Fatal("Error: ", err)
		}
		log.Println("Creating [" + pagedir + "trash] as trash")
	}
}

// Main function which powers up the wiki
func main() {
	flag.StringVar(&defaultlistenaddr, "listen", "localhost:8080",
		"Specifies the  ip:port combination for the tkwiki http server")
	flag.StringVar(&tmpldir, "tmpldir", "wtmpl/",
		"Specify directory which holds the template files for building the webpages")
	flag.StringVar(&pagedir, "datadir", "data/",
		"Specify the folder where wiki data will be stored")
	flag.StringVar(&staticdir, "staticdir", "static/",
		"Specify static files folder (css/favicon/images)")

	flag.Parse()

	templates.Funcs(funcMap)
	_, err := templates.ParseGlob(tmpldir + "*.html")
	if err != nil {
		log.Println(
			"A template error has been occured: ", err)
	}

	log.Println("Starting tkwiki...")
	initDataDir()
	initTrash()

	http.HandleFunc("/view/", makeHandler(viewHandler))
	http.HandleFunc("/edit/", makeHandler(editHandler))
	http.HandleFunc("/save/", makeHandler(saveHandler))
	http.HandleFunc("/delete/", makeHandler(deleteHandler))
	http.HandleFunc("/info/", makeHandler(metadataHandler))
	http.HandleFunc("/", rootPage)

	http.Handle("/static/", http.StripPrefix("/static/",
		http.FileServer(http.Dir(staticdir))))

	log.Println("tkwiki listens at: \"http://" + defaultlistenaddr + "\"")
	log.Fatal(http.ListenAndServe(defaultlistenaddr, nil))
}
